# 命名規則はMindBEMding
Block__Element--Modifire

# 設計はFLOCSSベース

## Foundation
リセットや変数など

## Layout
flexやfloatなど

## Object

### Component
繰り返し使われる最小単位

### Project
Componentを制御する

### Utility
marginやpaddingなど

## クラス名のプレフィックス
layout - l-*
component - o-*
utility - u-*

#EJSによるテンプレート化
EJSを使ってテンプレート化しています。
ルートへの相対パスが必要なため、pages.jsonにsiterootの記載が必要です。
ejs-gulpならgulpを使ってファイルパスが渡せるのですが、今回はnpm-script縛りをしているためejs-cliを使っています。
